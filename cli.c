#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "graph.h"
#include "fsgraph.h"
#include "tools.h"

/*
 * do not print intermediate nodes
 * print only important ones
 *
 */

const char *const fields[] = {
  "seclen", "tracks", "sectrk", "blocksize", "maxdir", "boottrk", "skew", "offset",
  "spt", "bsh", "blm", "exm", "dsm", "drm", "al0", "al1", "cks", "off", "psh", "prm",
  "fs_blocks", "trksz", "fs_bytes", "tot_bytes", "dir_blocks",
  "dir_start", "data_start", "img_bytes", NULL };

bool
is_print_field(field_name)
     char *field_name;
{
  int i = 0;
  while(fields[i] != NULL)
    {
      if (strcmp(field_name, fields[i]) == 0)
        {
          return true;
        }
      ++i;
    }
  return false;
}

void
printf_graph(graph,all)
     GRAPH graph;
     bool all;
{
  int i = 0;
  NODE *nodes = get_nodes(graph);
  while (i < GRAPH_SZ && nodes[i] != NULL)
    {
      struct node *node = nodes[i];
      char *identifier = get_identifier(node);
      if (all || is_print_field(identifier))
        {
          if (!node_empty(node))
            {
              printf("\t%s = %d (0x%02x)\n", identifier, get_value(node), get_value(node));
            }
          else
            {
              printf("\t%s undefined\n", identifier);
            }
        }
      ++i;
    }
}

char *
trim(str)
     char *str;
{
  char *s = str;
  while (*str == ' ' || *str == '\t' || *str == '\n' || *str == 'r')
    {
      ++s;
    }
  return s;
}

int
split_param(str, p_id, p_val)
     char *str;
     char **p_id;
     int *p_val;
{
  *p_id = strtok(str, "=");
  if (*p_id != NULL)
    {
      char *str_val = strtok(NULL, "=");
      if (str_val != NULL)
        {
          *p_val = atoi(str_val);
          return 0;
        }
    }
  return -1;
}

int
load_params(graph, fn)
     GRAPH graph;
     char *fn;
{
  FILE *f = fopen(fn, "r");
  if (f == NULL)
    {
      return -1;
    }
  char line[20];
  while (fgets(line, 20, f) != NULL)
    {
      char *identifier;
      int value;
      if (split_param(line, &identifier, &value) != 0)
        return -1;
      set_node_value(graph, identifier, value);
    }
  return 0;
}

void
print_in_usage()
{
  printf("commands:\n"
         "  <label>=<value>: set value for the node label\n"
         "  p: print values\n"
         "  a: print all values\n"
         "  l <file>: load file with values\n"
         "  e <file>: export diskdef to file\n"
         "  a <file>: export asm dpb to file\n"
         "  q: exit\n");
}

void
print_out_usage(str)
     char *str;
{
  printf("usage: %s <filename>\nor: %s\n", str, str);
}

int
main(argc, argv)
     int argc;
     char ** argv;
{
  GRAPH graph = build_graph();

  if (argc == 2)
    {
      if (load_params(graph, argv[1]) != 0)
        {
          fprintf(stderr, "error loading from file\n");
          exit(-1);
        }
    }
  else if (argc >2)
    {
      print_out_usage(argv[0]);
      exit(0);
    }

  char *str;
  while (true)
    {
      str = readline(">");
      if (strcmp(str, "p") == 0)
        {
          printf_graph(graph, false);
        }
      else if (strcmp(str, "a") == 0)
        {
          printf_graph(graph, true);
        }
      else if (str[0] == 'l' && str[1] == ' ')
        {
          char *fn = trim(str + 2);
          load_params(graph, fn);
        }
      else if (strcmp(str, "q") == 0)
        {
          break;
        }
      else if (str[0] == 'e' && str[1] == ' ')
        {
          char *s = trim(str + 2);
          if (write_diskdef(stdout, graph, s) == 0)
            {
              printf("Export Diskdef: OK\n");
            }
          else
            {
              printf("Export Diskdef ERROR\n");
            }
        }
      else if (strcmp(str, "a") == 0)
        {
          if (write_dpb(stdout, graph) == 0)
            {
              printf("Export DPB OK\n");
            }
          else
            {
              printf("Export DPB ERROR\n");
            }
        }
      else
        {
          char *identifier;
          int value;
          if (split_param(str, &identifier, &value) == 0)
            {
              set_node_value(graph, identifier, value);
            }
          else
            {
              print_in_usage();
            }
        }
      free(str);
    }
  free(str);
  free_graph(graph);
  return 0;
}
