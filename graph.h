#ifndef GRAPH_H
#define GRAPH_H

#include <stdbool.h>

#define GRAPH_SZ 100


typedef struct graph *GRAPH;

typedef struct node *NODE;

typedef struct fnode *FNODE;

char *get_identifier(NODE);

bool node_empty(NODE);

int get_value(NODE);

NODE *get_nodes(GRAPH);


/* 48 */
GRAPH init_graph();

/* 59 */
NODE add_node(GRAPH, char *);

/* 90 */
FNODE make_fnode(NODE, int (*f)(), int);

/* 123 */
void add_edge(NODE, FNODE,  int);

/* 148 */
void free_graph(GRAPH);

/* 196 */
NODE node_by_identifier(GRAPH, char *);

/* 266 */
void set_node_value(GRAPH, char *, int);


#endif /* GRAPH_H */
