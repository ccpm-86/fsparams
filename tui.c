#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <ncurses.h>
#include <form.h>
#include <limits.h>

#include "graph.h"
#include "fsgraph.h"
#include "tools.h"

#define FIELD_LIST_MAXSZ 100
#define FIELD_WIDTH 7


void
printw_graph(graph)
     GRAPH graph;
{
  move(0,0);
  int i = 0;
  NODE *nodes = get_nodes(graph);
  while (i < GRAPH_SZ && nodes[i] != NULL)
    {
      struct node *node = nodes[i];
      if (!node_empty(node))
        {
          printw("\t%s = %d\n", get_identifier(node), get_value(node));
        }
      else
        {
          printw("\t%s undefined\n", get_identifier(node));
        }
      ++i;
    }
}

struct param {
  char *identifier;
  char *label;
  bool editable;
};

const struct param diskdef_params[] = {
  {"seclen", "seclen", true},
  {"tracks", "tracks", true},
  {"sectrk", "sectrk", true},
  {"blocksize", "blocksize", true},
  {"maxdir", "maxdir", true},
  {"boottrk", "boottrk", true},
  {"skew", "skew", true},
  {"offset", "offset", true}
};

const struct param dpb_params[] = {
  {"spt", "spt", true},
  {"bsh", "bsh", true},
  {"blm", "blm", true},
  {"exm", "exm", false},
  {"dsm", "dsm", true},
  {"drm", "drm", true},
  {"al0", "al0", false},
  {"al1", "al1", false},
  {"cks", "cks", false},
  {"off", "off", true},
  {"psh", "psh", true},
  {"prm", "prm", true}
};

const struct param val_params[] = {
  {"tot_bytes", "Total Bytes", false},
  {"dir_start", "Directory Offset", false},
  {"data_start", "Data Offset", false}
};

int field_array_sz = 0;
FIELD *field_array[FIELD_LIST_MAXSZ];
char *field_names[FIELD_LIST_MAXSZ];

struct win_data {
  char *title;
  struct param *params;
  WINDOW *win;
  FORM *form;
  FIELD **fields;
};

int get_length(array)
     char **array;
{
  int i = 0;
  while (array[i] != NULL)
    {
      ++i;
    }
  return i;
}

void
make_win(wd, title, n_params, params, top, left)
     int n_params;
     struct win_data *wd;
     char *title;
     struct param *params;
     int top;
     int left;
{
  const int height = n_params + 4;
  int max_label_length = 0;
  for (int i = 0; i < n_params; ++i)
    {
      int label_length = strlen(params[i].label);
      if (label_length > max_label_length)
        {
          max_label_length = label_length;
        }
    }
  const int width = max_label_length + FIELD_WIDTH + 6;

  int title_length = strlen(title);
  wd -> title = malloc(title_length + 1);
  wd -> params = params;
  strcpy(wd -> title, title);
  wd -> win = newwin(height, width, top, left);
  keypad(wd -> win, TRUE);
  box(wd -> win, 0, 0);
  mvwprintw(wd -> win, 1, width / 2 - title_length / 2, "%s", title);
  mvwaddch(wd -> win, 2, 0, ACS_LTEE);
  whline(wd -> win, 0, width - 2);
  mvwaddch(wd -> win, 2, width - 1, ACS_RTEE);

  wd -> fields = malloc(sizeof(FIELD *) * (n_params + 1));

  for (int i = 0; i < n_params; ++i)
    {
      const char *identifier = params[i].identifier;
      wd -> fields[i] = new_field(1, FIELD_WIDTH,
                                  i + 0, 3 + max_label_length, 0, 0);
      if (params[i].editable)
        {
          set_field_back(wd -> fields[i], A_UNDERLINE);
        }
      else
        {
          field_opts_off(wd -> fields[i], O_EDIT);
          set_field_back(wd -> fields[i], A_NORMAL);
        }
      field_opts_off(wd -> fields[i], O_AUTOSKIP);
      set_field_type(wd -> fields[i], TYPE_INTEGER, 0, 0, INT_MAX);
      field_array[field_array_sz] = wd -> fields[i];
      field_names[field_array_sz] = malloc(strlen(identifier) + 1);
      strcpy(field_names[field_array_sz], identifier);
      ++field_array_sz;
    }
  wd -> fields[n_params] = NULL;

  wd -> form = new_form(wd -> fields);
  set_form_win(wd ->form, wd -> win);
  set_form_sub(wd -> form, derwin(wd -> win, height - 4, width - 2, 3, 1));
  post_form(wd -> form);

  for (int i = 0; i < n_params; ++i)
    {
      mvwprintw(wd -> win, i + 3, 2, "%s", params[i].label);
    }
  wrefresh(wd -> win);
}

int                             /* 0 if updated */
validate_current(graph, wd)
     GRAPH graph;
     struct win_data *wd;
{
  form_driver(wd -> form, REQ_VALIDATION);
  FIELD *field = current_field(wd -> form);
  if (field_status(field) == 0)
    return -1;
  int value = atoi(field_buffer(field, 0));
  char *identifier = wd -> params[field_index(field)].identifier;
  set_node_value(graph, identifier, value);
  return 0;
}

void
update_fields(graph)
     GRAPH graph;
{
  for (int i = 0; i < field_array_sz; ++i)
    {
      FIELD *fld = field_array[i];
      char *lab = field_names[i];
      NODE node = node_by_identifier(graph, lab);
      char sval[10];
      if (!node_empty(node))
        {
          int val = get_value(node);
          snprintf(sval, 10, "%d", val);
          sval[9] = 0;
          set_field_buffer(fld, 0, sval);
        }
    }
}

void
refresh_wins(ref_wins)
     WINDOW *ref_wins[];
{
  int i = 0;
  while (ref_wins[i] != NULL)
    {
      wrefresh(ref_wins[i]);
      ++i;
    }
}

void
validate(graph, wd)
     GRAPH graph;
     struct win_data *wd;
{
  if (validate_current(graph, wd) == 0)
    {
      update_fields(graph);
    }
}

void
status(s, win)
     WINDOW *win;
     char *s;
{
  mvwprintw(win, 0, 0, "%s", s);
  wclrtoeol(win);
  wrefresh(win);
}

int
main(argc, argv)
     int argc;
     char **argv;
{
  initscr();
  cbreak();
  noecho();
  raw();
  //  keypad(stdscr, TRUE);
  struct graph *graph = build_graph();

  struct win_data diskdef_wd;
  make_win(&diskdef_wd, "diskdef",
           ARRAY_SIZE(diskdef_params), diskdef_params, 3, 20);

  struct win_data dpb_wd;
  make_win(&dpb_wd, "dpb",
           ARRAY_SIZE(dpb_params), dpb_params, 3, 50);

  struct win_data val_wd;
  make_win(&val_wd, "Values",
           ARRAY_SIZE(val_params), val_params, 15, 5);

  WINDOW *status_win = newwin(1, 80, 0, 0);

  WINDOW *ref_wins[] = {diskdef_wd.win, dpb_wd.win, val_wd.win, NULL};

  struct win_data *wd = &diskdef_wd;

  form_driver(wd -> form, REQ_FIRST_FIELD);
  char *default_status = "^A: export asm dpb, ^E: export diskdef";
  status(default_status, status_win);
  int ch = wgetch(wd -> win);
  while (ch != 3 && ch != 27)
    {
      char *status_line = default_status;
      switch (ch)
        {
        case KEY_DOWN:
          validate(graph, wd);
          form_driver(wd -> form, REQ_NEXT_FIELD);
          FIELD *field_ = current_field(wd -> form);
          while ((field_opts(field_) & O_EDIT) == 0)
            {
              form_driver(wd -> form, REQ_NEXT_FIELD);
              field_ = current_field(wd -> form);
            }
          break;
        case KEY_UP:
          validate(graph, wd);
          form_driver(wd -> form, REQ_PREV_FIELD);
          FIELD *fld = current_field(wd -> form);
          while ((field_opts(fld) & O_EDIT) == 0)
            {
              form_driver(wd -> form, REQ_PREV_FIELD);
              fld = current_field(wd -> form);
            }
          break;
        case KEY_LEFT:
          form_driver(wd -> form, REQ_PREV_CHAR);
          break;
        case KEY_RIGHT:
          form_driver(wd -> form, REQ_NEXT_CHAR);
          break;
        case KEY_DC:
          form_driver(wd -> form, REQ_DEL_CHAR);
          break;
        case KEY_BACKSPACE:
          form_driver(wd -> form, REQ_DEL_PREV);
          break;
        case 1:                 /* ^A: dpb export */
          {
            FILE *f = fopen("dpb.a86", "w");
            write_dpb(f, graph);
            fclose(f);
            status_line = "export dpb ok";
          }
          break;
        case 5:                 /* ^E: diskdef export */
          {
            FILE *f = fopen("diskdef", "w");
            write_diskdef(f, graph, "disk");
            fclose(f);
            status_line = "export diskdef ok";
          }
          break;
        case 9:                 /* TAB */
          validate(graph, wd);
          if (wd == &diskdef_wd)
            {
              wd = &dpb_wd;
            }
          else
            {
              wd = &diskdef_wd;
            }
          form_driver(wd -> form, REQ_FIRST_FIELD);
          break;
        case 10:
          validate(graph, wd);
          form_driver(wd -> form, REQ_BEG_FIELD);
          break;
        default:
          form_driver(wd -> form, ch);
        }
      status(status_line, status_win);
      refresh_wins(ref_wins);
      form_driver(wd -> form, REQ_INS_MODE);
      ch = wgetch(wd -> win);
    }

  free_graph(graph);

  endwin();

  return 0;
}
