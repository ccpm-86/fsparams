#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "graph.h"

#define ADJ_SZ 10
#define REC_MAX 100
#define PARAM_SZ 10

struct graph
{
  struct node *nodes[GRAPH_SZ];
  struct fnode *fnodes[GRAPH_SZ];
};

struct node
{
  struct graph *graph;
  char *identifier;
  bool is_empty;
  int value;
  struct edge *adj[ADJ_SZ];
};

struct fnode
{
  struct node *target_node;
  int (*f)();
  int nparam;
  struct node *nodes[PARAM_SZ];
};

struct edge
{
  struct node *node_from;
  struct fnode *fnode;
  int param_idx;
};

char *
get_identifier(node)
     struct node *node;
{
  return node -> identifier;
}

bool
node_empty(node)
     struct node *node;
{
  return node -> is_empty;
}

int
get_value(node)
     struct node *node;
{
  return node -> value;
}

struct node **
get_nodes(graph)
     struct graph *graph;
{
  return graph -> nodes;
}

struct graph *
init_graph()
{
  struct graph *graph = malloc(sizeof (struct graph));
  memset(graph -> nodes, 0, sizeof graph -> nodes);
  memset(graph -> fnodes, 0, sizeof graph -> fnodes);
  return graph;
}

struct node *
add_node(graph, identifier)
     struct graph *graph;
     char *identifier;
{
  struct node *node = malloc(sizeof(struct node));
  char *node_identifier = malloc(strlen(identifier) + 1);
  strcpy(node_identifier, identifier);
  node -> graph = graph;
  node -> identifier = node_identifier;
  node -> is_empty = true;
  memset(node -> adj, 0, sizeof node -> adj);

  /* insert node into graph */
  int i = 0;
  while (i < GRAPH_SZ && graph -> nodes[i] != NULL)
    {
      ++i;
    }
  if (i < GRAPH_SZ)
    {
      graph -> nodes[i] = node;
    }
  else
    {
      exit(1);
    }
  return node;
}

struct fnode*
make_fnode(target_node, f, nparam)
     struct node *target_node;
     int (*f)();
     int nparam;
{
  struct fnode *fnode = malloc(sizeof (struct fnode));
  fnode -> target_node = target_node;
  fnode -> f = f;
  fnode -> nparam = nparam;

  /* insert fnode into graph */
  int i = 0;
  struct graph *graph = target_node -> graph;
  while (i < GRAPH_SZ && graph -> nodes[i] != NULL)
    {
      ++i;
    }
  if (i < GRAPH_SZ)
    {
      graph -> fnodes[i] = fnode;
    }
  else
    {
      exit(1);
    }
return fnode;
}

void
add_edge(node_from, fnode, param_idx)
     struct node *node_from;
     struct fnode *fnode;
     int param_idx;
{
  struct edge *e = malloc(sizeof(struct edge));
  //  e -> node_from = node_from;
  e -> fnode = fnode;
  e -> param_idx = param_idx;
  fnode -> nodes[param_idx] = node_from;

  /* node_from: add e to adj */
  int i = 0;
  while (i < ADJ_SZ && node_from -> adj[i] != NULL)
    {
      ++i;
    }
  if (i < ADJ_SZ)
    {
      node_from -> adj[i] = e;
    }
  else
    {
      exit(-1); /* error: too many outgoing edges, hope it will never happen */
    }
}

void
free_graph(graph)
     struct graph *graph;
{
  int i = 0;
  while (i < GRAPH_SZ && graph -> nodes[i] != NULL)
    {
      struct node *node = graph -> nodes[i];
      free(node -> identifier);
      int j = 0;
      while (j < ADJ_SZ && node -> adj[j] != NULL)
        {
          free(node -> adj[j]);
          ++j;
        }
      free(graph -> nodes[i]);
      ++i;
    }
  int j = 0;
  while (j < GRAPH_SZ && graph -> nodes[j] != NULL)
    {
      free(graph -> fnodes[j]);
      ++j;
    }
}

struct node *
node_by_identifier(graph, identifier)
     struct graph *graph;
     char *identifier;
{
  int i = 0;
  while (i < GRAPH_SZ && graph -> nodes[i] != NULL)
    {
      struct node *node = graph -> nodes[i];
      if (strcmp(identifier, node -> identifier) == 0)
        {
          return node;
        }
      ++i;
    }
  return NULL;
}

void set_fnode(struct fnode *, int, int);
int rec_count;
void
rec_set_node_value(node, value)
     struct node *node;
     int value;
{
  ++rec_count;
  if (rec_count >= REC_MAX)
    {
      fprintf(stderr, "recursion error\n");
      exit(1);
    }
  if (!node -> is_empty && node -> value == value)
    {
      return;
    }

  node -> value = value;
  node -> is_empty = false;
  int i = 0;
  while (i < ADJ_SZ && node -> adj[i] != NULL)
    {
      struct edge *edge = node -> adj[i];
      set_fnode(edge -> fnode, value, edge -> param_idx);
      ++i;
    }
}

int
apply(f, n, nodes)
     int (*f)();
     int n;
     struct node *nodes[];
{
  switch (n)
    {
    case 0:
      return f();
    case 1:
      //printf("\t%s=%d\n", nodes[0] -> identifier, nodes[0] -> value);
      return f(nodes[0] -> value);
    case 2:
      //printf("\t%s=%d, %s:%d\n", nodes[0] -> identifier, nodes[0] -> value,
             //nodes[1] -> identifier, nodes[1] -> value);
      return f(nodes[0] -> value, nodes[1] -> value);
    case 3:
      return f(nodes[0] -> value, nodes[1] -> value, nodes[2] -> value);
    case 4:
      return f(nodes[0] -> value, nodes[1] -> value, nodes[2] -> value, nodes[3] -> value);
    }
  exit(-1);
}

void
set_fnode(fnode, value, param_idx)
     struct fnode *fnode;
     int value;
     int param_idx;
{
  int i = 0;
  int cnt = 0;
  while (i < PARAM_SZ && i < fnode -> nparam)
    {
      if (!fnode -> nodes[i] -> is_empty)
        {
          ++cnt;
        }
      ++i;
    }
  if (cnt == fnode -> nparam)
    {
      //printf("target:%s\n", fnode -> target_node -> identifier);
      int v = apply(fnode -> f, fnode -> nparam, fnode -> nodes);
      rec_set_node_value(fnode -> target_node, v);
    }
}

void
set_node_value(graph, identifier, value)
     struct graph *graph;
     char *identifier;
     int value;
{
  struct node *node = node_by_identifier(graph, identifier);
  if (node != NULL)
    {
      rec_count = 0;
      rec_set_node_value(node, value);
    }
}
