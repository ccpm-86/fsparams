#include <stdio.h>

#include "graph.h"
#include "fsgraph.h"
#include "tools.h"

const char *const diskdef[] = { "seclen", "tracks", "sectrk", "blocksize",
                                "maxdir", "boottrk", "skew", "offset"};

struct dpbdef {
  char *label;
  int sz;
  char *comment;
};

const struct dpbdef dpb[] = {
  {"spt", 2, "Sectors Per Track"},
  {"bsh", 1, "Block Shift"},
  {"blm", 1, "Block Mask"},
  {"exm", 1, "Extnt Mask"},
  {"dsm", 2, "Disk Size - 1"},
  {"drm", 2, "Directory Max"},
  {"al0", 1, "Alloc0"},
  {"al1", 1, "Alloc1"},
  {"cks", 2, "Check Size"},
  {"off", 2, "Offset"},
  {"psh", 1, "Phys Sec Shift"},
  {"prm", 1, "Phys Rec Mask"}
};

int
get_node_value(graph, identifier, pval)
     GRAPH graph;
     char *identifier;
     int *pval;
{
  NODE node = node_by_identifier(graph, identifier);
  if (node == NULL || node_empty(node))
    {
      return -1;
    }
  *pval = get_value(node);
  return 0;
}

int
write_diskdef(file, graph, name)
     FILE *file;
     GRAPH graph;
     char *name;
{
  fprintf(file, "diskdef %s\n", name);
  for (int i = 0; i < ARRAY_SIZE(diskdef); ++i)
    {
      int value;
      if (get_node_value(graph, diskdef[i], &value) == 0)
        {
          fprintf(file, "  %s %d\n", diskdef[i], value);
        }
      else
        {
          fprintf(file, "  #%s: no value \n", diskdef[i]);
        }
    }
  fprintf(file, "end\n");
  return 0;
}

int
write_dpb(file, graph)
     FILE *file;
     GRAPH graph;
{
  int addr = 0;
  for (int i = 0; i < ARRAY_SIZE(dpb); ++i)
    {
      char *label = dpb[i].label;
      char *s = dpb[i].sz == 1 ? "byte" : "word";
      fprintf(file, "%s\tequ\t%s ptr %d\n", label, s, addr);
      addr += dpb[i].sz;
    }
  fprintf(file, "\n");

  fprintf(file, "dpb0\tequ\toffset $\t;DiskParameter Block\n");
  for (int i = 0; i < ARRAY_SIZE(dpb); ++i)
    {
      int value;
      char *comment = dpb[i].comment;
      if (get_node_value(graph, dpb[i].label, &value) == 0)
        {
          char *defsz = dpb[i].sz == 1 ? "db" : "dw";
          fprintf(file, "\t%s\t%d\t\t;%s\n", defsz, value, comment);
        }
      else
        {
          char *defsz = dpb[i].sz == 1 ? "rb" : "rw";
          fprintf(file, "\t%s\t1\t\t;%s\n", defsz, comment);
        }
    }
  fprintf(file, "\n");
  return 0;
}
