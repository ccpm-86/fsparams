#ifndef TOOLS_H
#define TOOLS_H

#include "graph.h"

#define ARRAY_SIZE(array) (sizeof(array)/sizeof(array[0]))

int write_diskdef(FILE *, GRAPH, char *);
int write_dpb(FILE *, GRAPH);

#endif /* TOOLS_H */
