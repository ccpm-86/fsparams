.PHONY: all clean

CC = gcc
CFLAGS = -Wall -g -lncurses -lform -lreadline

SRC = graph.c fsgraph.c tools.c
EXE = cli tui

all: $(EXE)

%: %.c $(SRC)
	$(CC) $^ $(CFLAGS) -o $@

clean:
	rm -f $(wildcard *.o) $(wildcard *~) $(wildcard a.out) $(EXE)
