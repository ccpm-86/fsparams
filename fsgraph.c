#include <stdint.h>
#include <stdio.h>

#include "graph.h"

/*** Common Functions ***/

int
lg2(x)
     int x;
{
  return sizeof(int)*8 - 1 - __builtin_clz(x);
}

int
id(x)
     int x;
{
  return x;
}

int
add1(x)
     int x;
{
  return x + 1;
}

int
sub1(x)
     int x;
{
  return x - 1;
}

int
fnadd(x, y)
     int x;
     int y;
{
  return x + y;
}

int
fnsub(x, y)
     int x;
     int y;
{
  return x - y;
}

int
fnmul(x, y)
     int x;
     int y;
{
  return x * y;
}

int
fndiv(x, y)
     int x;
     int y;
{
  return x / y;               /* floor */
}

int
fndivceil(x, y)
     int x;
     int y;
{
  return (x + y - 1) / y;     /* ceiling */
}


/*** Fields functions ***/

int
bls2bsh(bls)
     int bls;
{
  return lg2(bls) - 7; /* bsh = log2(bls) - 7 */
}

int
bsh2bls(bsh)
     int bsh;
{
  return 1 << (bsh + 7);     /* bls = 2^(bsh + 7) */
}

int
blm2bsh(blm)
     int blm;
{
  return lg2(blm + 1);       /* bsh = log2(blm + 1) */
}

int
bsh2blm(bsh)
     int bsh;
{
  return (1 << bsh) - 1;     /* blm = 2^bsh - 1 */
}

int
dsm2entryblocks(dsm)
     int dsm;
{
  return dsm <= 255 ? 16 : 8;
}

int
bsh2exm(bsh, entry_blocks)
     int bsh;
     int entry_blocks;
{
  return (1 << (bsh + lg2(entry_blocks) - 7)) - 1;
}

int
bls2epb(bls)
     int bls;
{
  return bls / 32;
}

int
epb2bls(epb)
     int epb;
{
  return epb * 32;
}

int
epb2dirblocks(epb, maxdir)
     int epb;
     int maxdir;
{
  return (maxdir + epb - 1) / epb;
}

int
al2dirblocks(al0, al1)
     uint8_t al0;
     uint8_t al1;
{
  uint16_t a = ~(al0*0x100 + al1) + 1;
  return 16 - lg2(a);
}

int
dirblocks2al0(dirblocks)
     int dirblocks;
{
  uint16_t a = 1 << (16 - dirblocks);
  uint16_t b = ~(a-1);
  return b >> 8;
}

int
dirblocks2al1(dirblocks)
     int dirblocks;
{
  uint16_t a = 1 << (16 - dirblocks);
  uint16_t b = ~(a-1);
  return b & 0xff;
}

int
drm2cks(drm)
     int drm;
{
  return drm / 4 + 1;
}

int
seclen2psh(seclen)
     int seclen;
{
  return lg2(seclen) - 7;
}

int
psh2seclen(psh)
     int psh;
{
  return 1 << (psh + 7);
}

int
prm2psh(prm)
     int prm;
{
  return lg2(prm + 1);       /* psh = log2(prm + 1) */
}

int
psh2prm(psh)
     int psh;
{
  return (1 << psh) - 1;     /* prm = 2^psh - 1 */
}

void
func_two(result_node, arg0_node, arg1_node, f)
     struct node *result_node;
     struct node *arg0_node;
     struct node *arg1_node;
     int (*f)();
{
  struct fnode *fnode = make_fnode(result_node, f, 2);
  add_edge(arg0_node, fnode, 0);
  add_edge(arg1_node, fnode, 1);
}

void
monofun(node1, node2, f)    /* f: node1 -> node2 */
     struct node *node1;
     struct node *node2;
     int (*f)();
{
  add_edge(node1, make_fnode(node2, f, 1), 0);
}

void
bifun(node1, node2, f, g) /* f:node1->node2, g:node2->node1 */
     struct node *node1;
     struct node *node2;
     int (*f)();
     int (*g)();
{
  add_edge(node1, make_fnode(node2, f, 1), 0);
  add_edge(node2, make_fnode(node1, g, 1), 0);
}

void
addfun(sum, node1, node2)
     struct node *sum, *node1, *node2;
{
  func_two(sum, node1, node2, fnadd);  /* sum   = node1 + node2 */
  func_two(node1, sum, node2, fnsub);  /* node1 = sum   - node2 */
  func_two(node2, sum, node1, fnsub);  /* node2 = sum   - node1 */
}

void
mulfun(prod, node1, node2)
     struct node *prod, *node1, *node2;
{
  func_two(prod, node1, node2, fnmul);  /* prod = node1 * node2 */
  func_two(node1, prod, node2, fndiv);  /* node1 = prod / node2 */
  func_two(node2, prod, node1, fndiv);  /* node2 = prod / node1 */
}

struct graph *
build_graph()
{
  struct graph *graph = init_graph();

  /* diskdefs */
  struct node *seclen = add_node(graph, "seclen");
  struct node *tracks = add_node(graph, "tracks");
  struct node *sectrk = add_node(graph, "sectrk");
  struct node *blocksize = add_node(graph, "blocksize");
  struct node *maxdir = add_node(graph, "maxdir");
  struct node *boottrk = add_node(graph, "boottrk");
  struct node *skew = add_node(graph, "skew");
  (void) skew; /* TODO: do something with it */
  struct node *offset = add_node(graph, "offset");

  /* dpb */
  struct node *spt = add_node(graph, "spt");
  struct node *bsh = add_node(graph, "bsh");
  struct node *blm = add_node(graph, "blm");
  struct node *exm = add_node(graph, "exm");
  struct node *dsm = add_node(graph, "dsm");
  struct node *drm = add_node(graph, "drm");
  struct node *al0 = add_node(graph, "al0");
  struct node *al1 = add_node(graph, "al1");
  struct node *cks = add_node(graph, "cks");
  struct node *off = add_node(graph, "off");
  struct node *psh = add_node(graph, "psh");
  struct node *prm = add_node(graph, "prm");

  /* calculated values */
  struct node *fs_blocks = add_node(graph, "fs_blocks");
  struct node *trksz = add_node(graph, "trksz");
  struct node *ftrk = add_node(graph, "ftrk");
  struct node *fs_bytes = add_node(graph, "fs_bytes");
  struct node *tot_bytes = add_node(graph, "tot_bytes");
  struct node *alloc_bytes = add_node(graph, "alloc_bytes");
  struct node *dir_blocks = add_node(graph, "dir_blocks");
  struct node *entry_blocks = add_node(graph, "entry_blocks");
  struct node *epb = add_node(graph, "epb");
  struct node *dirsz = add_node(graph, "dirsz");
  struct node *dir_start = add_node(graph, "dir_start");
  struct node *data_start = add_node(graph, "data_start");
  struct node *res_bytes = add_node(graph, "res_bytes");
  struct node *img_bytes = add_node(graph, "img_bytes");

  /* sectrk = spt */
  bifun(sectrk, spt, id, id);

  /* trksz = seclen * sectrk */
  mulfun(trksz, seclen, sectrk);

  /* tot_bytes = tracks * trksz */
  mulfun(tot_bytes, tracks, trksz);

  /* alloc_bytes = tot_bytes - res_bytes */
  /* tot_bytes = alloc_bytes + res_bytes */
  addfun(tot_bytes, alloc_bytes, res_bytes);

  /* fs_blocks = alloc_bytes div bls */
  func_two(fs_blocks, alloc_bytes, blocksize, fndiv);

  /* fs_bytes = fs_blocks * bls */
  mulfun(fs_bytes, fs_blocks, blocksize);

  /* tracks = boottrk + ftrk */
  addfun(tracks, boottrk, ftrk);

  /* ftrk = ceiling (fs_bytes / trksz) */
  func_two(ftrk, fs_bytes, trksz, fndivceil);

  /* blocksize = 2^(bsh + 7), bsh = log2(blocksize) - 7 */
  bifun(blocksize, bsh, bls2bsh, bsh2bls);

  /* blm = 2^bsh - 1, bsh = log2(blm + 1) */
  bifun(blm, bsh, blm2bsh, bsh2blm);

  /* entry_blocks = dsm <= 255 ? 16 : 8 */
  monofun(dsm, entry_blocks, dsm2entryblocks);

  /* exm = (1 << (bsh + lg2(entry_blocks) - 7)) - 1 */
  func_two(exm, bsh, entry_blocks, bsh2exm);

  /* fs_blocks = dsm + 1, dsm = fs_blocks - 1 */
  bifun(fs_blocks, dsm, sub1, add1);

  /* maxdir = drm + 1, drm = maxdir - 1 */
  bifun(maxdir, drm, sub1, add1);

  /* epb = bls div 32*/
  bifun(epb, blocksize, epb2bls, bls2epb);

  /* dir_blocks = ceiling (maxdir / epb) */
  func_two(dir_blocks, epb, maxdir, epb2dirblocks);

  /* dir_blocks = 16-log2(~(al0*0x100 + al1) + 1 */
  func_two(dir_blocks, al0, al1, al2dirblocks);

  /* al0 = ~2^((16-dir_blocks) - 1) div 0x100 */
  monofun(dir_blocks, al0, dirblocks2al0);

  /* al1 = ~2^((16-dir_blocks) - 1) mod 0x100 */
  monofun(dir_blocks, al1, dirblocks2al1);

  /* cks = drm / 4 + 1 (monodirectional)*/
  monofun(drm, cks, drm2cks);

  /* boottrk = off */
  bifun(boottrk, off, id, id);

  /* res_bytes = boottrk * trksz */
  func_two(res_bytes, boottrk, trksz, fnmul);
  func_two(boottrk, res_bytes, trksz, fndiv);

  /* seclen = 2^(psh+7), psh = lg2(seclen) - 7 */
  bifun(seclen, psh, seclen2psh, psh2seclen);

  /* prm = 2^psh - 1, psh = log2(prm + 1) */
  bifun(prm, psh, prm2psh, psh2prm);

  /* dir_start = offset + res_bytes */
  addfun(dir_start, offset, res_bytes);

  /* dirsz = dir_blocks * blocksize */
  mulfun(dirsz, dir_blocks, blocksize);

  /* data_start = dir_start + dirsz */
  addfun(data_start, dir_start, dirsz);

  /* img_bytes = tot_bytes + offset */
  addfun(img_bytes, tot_bytes, offset);

  /* skew 0 by default unless modified */
  set_node_value(graph, "skew", 0);

  /* offset 0 by default unless modified */
  set_node_value(graph, "offset", 0);

  return graph;
}
